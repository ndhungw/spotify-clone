import axios from 'axios';
import { BASE_URI_API } from '../config';

const request = (method, path, data) => {
  const headers = {};

  return axios({
    method,
    url: BASE_URI_API + path,
    data,
    headers,
  });
};

export const get = (path) => request('get', path);

export const post = (path, data) => request('post', path, data);

export const del = (path) => request('delete', path);

export const put = (path, data) => request('put', path, data);

export const patch = (path, data) => request('patch', path, data);
