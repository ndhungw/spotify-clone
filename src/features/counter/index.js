import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import MainPage from './pages/Main';
import EditCounterPage from './pages/EditCounterPage';
import NotFound from '../../components/NotFound';

export default function Counter() {
  const match = useRouteMatch();

  return (
    <Switch>
      <Route exact path={match.url}>
        <MainPage />
      </Route>
      <Route path={`${match.url}/edit`}>
        <EditCounterPage />
      </Route>

      <Route>
        <NotFound />
      </Route>
    </Switch>
  );
}
