import React, { Suspense } from 'react';

import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import NotFound from './components/NotFound';

const Counter = React.lazy(() => import('./features/Counter'));

function App() {
  return (
    <>
      <Suspense fallback={<div>Loading ...</div>}>
        <Router>
          <Switch>
            <Redirect exact from="/" to="/counter" />

            <Route path="/counter">
              <Counter />
            </Route>
            <Route>
              <NotFound />
            </Route>
          </Switch>
        </Router>
      </Suspense>
    </>
  );
}

export default App;
