import { configureStore } from '@reduxjs/toolkit';
import counterReducer from '../features/Counter/slice';

export const store = configureStore({
  reducer: {
    counter: counterReducer,
  },
});
